# mjwall-openjdk

Just a docker container with the RHEL based Oracle Linux OS, OpenJDK 11 and some troubleshooting tools like lsof and wget already installed

## Build 

```
docker build -t mjwall/mjwall-openjdk:11.0-jdk
```

## Run

```
docker run --rm -it mjwall/mjwall-openjdk:11.0-jdk bash
```

## Move image

Copy it with 

```
docker save -o <path for generated tar file> <image name>
```

then load it with

```
docker load -i <path to image tar file>
```
