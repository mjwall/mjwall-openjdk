FROM adoptopenjdk/openjdk11:jdk-11.0.4_11-alpine

LABEL maintainer="mjwall@gmail.org"
LABEL description="Alpine based openjdk 11 and some tools for troubleshooting"
LABEL build="docker build -t mjwall/mjwall-openjdk:11.0-jdk"
LABEL run="docker run --rm  mjwall/mjwall-openjdk:11.0-jdk"
LABEL source="https://gitlab.com/mjwall/mjwall-openjdk"

RUN apk update && apk add gcc gdb make openssh && rm -rf /var/cache/apk/*

CMD ["jshell"]

