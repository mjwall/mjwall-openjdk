VERSION=11.0-jdk
BASE_NAME=mjwall/mjwall-openjdk
IMAGE_NAME=$(BASE_NAME):$(VERSION)

default: build

.PHONY: build tag_latest run

build:
	@docker build -t ${IMAGE_NAME} .

tag_latest:
	@docker tag ${IMAGE_NAME} ${BASE_NAME}:latest

run:
	@docker run --rm -it ${IMAGE_NAME} sh
